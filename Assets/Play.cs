﻿using UnityEngine;
using System.Collections;

public class Play : MonoBehaviour {

	IEnumerator Start () {
        // AssetBundleは自分のVersionで作り直しておいてください。
        var projectFolder = System.IO.Directory.GetCurrentDirectory() + "/AssetBundles/sound";

        //Windows向け何でmacはよしなに
        var path = "file://" + projectFolder;
	    path = path.Replace("\\", "/");
        WWW www = new WWW(path);
        yield return www;

        var assetBundleCreateRequest = AssetBundle.CreateFromMemory(www.bytes);
        yield return assetBundleCreateRequest;
        AssetBundle assetBundle = assetBundleCreateRequest.assetBundle;

        var clip = assetBundle.LoadAsset<AudioClip>("arp");
        Debug.LogFormat("loadState: {0}, loadType:{1}, preloadAudioData:{2}, loadInBackground:{3}", clip.loadState, clip.loadType, clip.preloadAudioData, clip.loadInBackground);
	    var source = gameObject.AddComponent<AudioSource>();
	    source.playOnAwake = false;
	    clip.LoadAudioData();

        source.clip = clip;
        source.Play();
	}
}
